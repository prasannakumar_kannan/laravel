<?php

use Illuminate\Database\Seeder;
use App\open_jobs;

class jobsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        open_jobs::truncate();
        $fake = \Faker\Factory::create();

        for($i=0;$i<5;$i++){
          open_jobs::create([
            'job_name' => $fake->sentence,
            'job_category'=>$fake->sentence,
                'match' => $fake->paragraph,
          ]);
        }
    }
}
